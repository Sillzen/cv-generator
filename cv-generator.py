import json
import sys
import os.path

#######################################

# JSON CV
cv_file = sys.argv[1] if len(sys.argv) > 1 else os.path.join(os.path.normpath(sys.path[0]), 'cv.json')

# Output directory
output_dir = sys.argv[2] if len(sys.argv) > 2 else '.'

# Formatting
nl  = '\n'
dl  = '\n\n'
sep = '---' + dl

#######################################

# Get & parse CV
cv_json = open(cv_file, 'r')
cv_data = json.load(cv_json)

# Open output files for editing
cv_md_light = open(os.path.join(os.path.normpath(output_dir), 'cv-light.md'), 'w+')
cv_md_full  = open(os.path.join(os.path.normpath(output_dir), 'cv-full.md'), 'w+')

# About
about   = cv_data['about']
name    = about['name']
web     = about['website']
email   = about['email']
gitlab  = about['git']['gitlab']
github  = about['git']['github']
summary = about['summary']

cv_md_light.write( '# ' + name + dl)
cv_md_full.write( '# ' + name + dl)

if web:
	cv_md_light.write('Web: [{0}]({0})'.format(web) + dl)
	cv_md_full.write('Web: [{0}]({0})'.format(web) + dl)

cv_md_light.write('Email: [{0}](mailto:{0})'.format(email) + dl)
cv_md_full.write('Email: [{0}](mailto:{0})'.format(email) + dl)

if gitlab:
	cv_md_light.write('GitLab: [{0}]({0})'.format(gitlab) + dl)
	cv_md_full.write('GitLab: [{0}]({0})'.format(gitlab) + dl)

if github:
	cv_md_light.write('GitHub: [{0}]({0})'.format(github) + dl)
	cv_md_full.write('GitHub: [{0}]({0})'.format(github) + dl)

if summary:
	cv_md_full.write(nl + sep)
	cv_md_full.write(summary + nl)

cv_md_light.write(nl + sep)
cv_md_full.write(nl + sep)


# Skills
skills = cv_data['skills'];

cv_md_light.write('## Skills' + dl)
cv_md_full.write('## Skills' + dl)
for skill in skills:
	cv_md_light.write('* ' + skill + nl)
	cv_md_full.write('* ' + skill + nl)

cv_md_light.write(nl + sep)
cv_md_full.write(nl + sep)


# Employment
employment = cv_data['employment']

cv_md_light.write('## Employment History' + dl)
cv_md_full.write('## Employment History' + dl)
for role in employment:
	company   = role['company_name']
	positions = role['positions']

	cv_md_light.write('### {0}'.format(company) + nl)
	cv_md_full.write('### {0}'.format(company) + nl)

	for position in positions:
		title  = position['job_title']
		start  = position['date_start']
		end    = position['date_end']
		desc   = position['description']
		duties = position['duties']

		cv_md_light.write('##### {0} ({1} - {2})'.format(title, start, end) + nl)
		cv_md_full.write('##### {0} ({1} - {2})'.format(title, start, end) + nl)

		cv_md_full.write(desc + nl)

		for duty in duties:
			cv_md_full.write('* {0}'.format(duty) + nl)

		cv_md_light.write(nl)
		cv_md_full.write(nl)

cv_md_full.write(sep)


# Interests
interests = cv_data['interests']

cv_md_full.write('## Interests' + dl)
for interest in interests:
	cv_md_full.write('* ' + interest + nl)


# Close all files
cv_md_light.close()
cv_md_full.close()
cv_json.close()