# cv-generator.py
Input JSON formatted CV, get markdown files. Outputs full and light (no summary, no role descriptions/duties, no interests) versions.

## Usage
`python cv-generator.py`
or
`python cv-generator.py path/to/cv.json`
or
`python cv-generator.py path/to/cv.json path/to/output/directory`

## Requirements
* Python 3+
